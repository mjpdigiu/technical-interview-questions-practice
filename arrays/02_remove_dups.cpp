#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

void printVec(vector<int> v){
	for(auto i : v)
		cout << i << " ";
	cout << endl;
}

vector<int> removeDuplicates(vector<int> v){
	// Sort, to group duplicates
	std::sort(v.begin(), v.end());

	vector<int> result;
	int prev = v.at(0);
	result.push_back(prev);
	for(int i = 1; i < v.size(); i++){
		int curr = v.at(i);
		if(curr != prev){
			result.push_back(curr);
			prev = curr;
		}
	}

	return result;
}

int main(int argc, char* argv[]){
	vector<int> v = {3, 10, 8, 6, 3, 12, 0, 0, 6, 6, 3};
	printVec(v);
	printVec(removeDuplicates(v));
}
