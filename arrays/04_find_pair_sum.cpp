#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

void printVec(vector<int> v){
	for(auto i : v)
		cout << i << " ";
	cout << endl;
}

void findPairsThatSumToN(vector<int> v, int n){
	// First, sort, for greedy n*logn solution
	std::sort(v.begin(), v.end());

	// Next, transform array to make it a 0 sum problem
	//  a + b = n  <=>  (2a - n) + (2b - n) = 0
	for(int i = 0; i < v.size(); i++)
		v[i] = 2*v[i] - n;

	// Start the indices at both ends of v and move towards the middle,
	//  printing pairs that sum to 0 (but print them converted back to
	//  their original values before the transformation)
	int a = 0;
	int b = v.size() - 1;
	while(a != b){
		int sum = v[a] + v[b];
		if(sum == 0){		// found valid pair
			cout << "(" << (v[a] + n)/2 << ", " << (v[b] + n)/2 << ")" << endl;
			// In case the array has duplicates of what a/b are pointing at
			//  we move them in as far as their duplicates go
			int aVal = v[a];
			while(a != b){
				a++;
				if(v[a] != aVal) break;
			}
			int bVal = v[b];
			while(a != b){
				b--;
				if(v[b] != bVal) break;
			}
		}
		else if(sum > 0)	// move b left to make sum smaller
			b--;
		else			// move a right to make sum bigger
			a++;
	}
	
	// Finally, once a and b meet, check if they are a valid pair (both 0)
}

int main(int argc, char* argv[]){
	vector<int> v = {20, 12, 6, 3, 13, 9, 5, 1, 5, 5, 13, 13, 9, 9, 9, 9};
	printVec(v);
	findPairsThatSumToN(v, 18);	
}
